<?php 
	// Conexão Local 
	$hostname = 'localhost'; 
	$username = 'grafico'; 
	$senha = 'grafico123'; 
	$banco = 'grafico';
	$conexao = new PDO("mysql:host=localhost;dbname=grafico", "grafico", "grafico123");	
	$meses = array(); 
	$valores = array(); 
	$i = 0;
	$rs = $conexao->query("SELECT EXTRACT(MONTH FROM ped_data) AS MES, sum(ped_valor) AS VALOR from PEDIDOS where EXTRACT(YEAR FROM ped_data) = 2019 group by EXTRACT(MONTH FROM ped_data)");
	while($row = $rs->fetch(PDO::FETCH_OBJ)){
		$meses[$i] = $row->MES; 
		$valores[$i] = $row->VALOR; 
		$i = $i + 1; 
	}
?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
	<head> 
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
		<title>Gráficos com Google Chart (1)</title> 
		<script type="text/javascript" src="https://www.google.com/jsapi"></script> 
		<script type="text/javascript"> 
			google.load('visualization', '1', {'packages':['corechart']}); 
			google.setOnLoadCallback(desenhaGrafico); 
			function desenhaGrafico() { 
				var data = new google.visualization.DataTable();
				data.addColumn('string', 'Mês'); 
				data.addColumn('number', 'Receitas em R$'); 
				data.addRows(<?php echo $i ?>); 
				<?php 
				$k = $i; 
				for ($i = 0; $i < $k; $i++) { 
					?> 
					data.setValue(<?php echo $i ?>, 0, '<?php echo $meses[$i] ?>'); 
					data.setValue(<?php echo $i ?>, 1, <?php echo $valores[$i] ?>); 
					<?php 
				} ?> 
				var options = { title: 'Montante dos Pedidos por Mês', width: 400, height: 300, colors: ['BLUE'], legend: { position: 'bottom' } }; 
				// cria grafico 
				var chart = new google.visualization.LineChart(document.getElementById('chart_div')); 
				// desenha grafico 
				chart.draw(data, options); 
			} 
		</script> 
	</head> 
	<body> 
		<h1>Faturamento com Pedidos em 2019</h1>
		<div id="chart_div"></div> 
	</body> 
</html>