<?php
require_once("classes/pdf.php");
require_once("classes/Usuarios.php");
require_once("classes/fpdf.php");

$usuario = new Usuarios();
session_start();
if (isset($_SESSION["usuario"]["nomeUsuario"])) {
  $idCliente = $_SESSION["usuario"]["idUsuario"];
  $usuario->setNome($_SESSION["usuario"]["nomeUsuario"]);
  $usuario->setEmail($_SESSION["usuario"]["emailUsuario"]);
} else {
  header("Location: erro.php?login=SIM");
}

$pdf = new PDF($usuario, 'P','mm','A4');

$header = array("Nome", "E-mail", "Apelido", "Celular", "Nascimento", "Tipo");

$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',8);

$pdf->FancyTable($header, $pdf->getContatos());
$pdf->Output();