<?php
require_once "Crud.php";

class TelefonesDAO extends Crud
{
    protected $table = "telefone";

    public function insert($telefone)
    { }

    public function insertTudo($id, $telefone)
    {
        $sql = "INSERT INTO $this->table (id, telefone) VALUES (:id, :telefone)";
        $stmt = DB::prepare($sql);
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $stmt->bindValue(":telefone", $telefone);

        return $stmt->execute();
    }

    public function update($id, $telefone)
    {
        $sql = "UPDATE $this->table SET telefone = :telefone WHERE id = :id";
        $stmt = DB::prepare($sql);
        $stmt->bindValue(":nome", $telefone->getTelefone());

        return $stmt->executeUpdate();
    }

    public function find($id){
        // :id = Significa que iremos utilizar a variável recebido como parâmetro.
        $sql = "SELECT * FROM $this->table WHERE id = :id";
        $stmt = DB::prepare($sql);
        $stmt->bindParam(":id", $id, PDO::PARAM_INT);
        $stmt->execute();

        return($stmt->fetchAll());
    }

    public function deleteTelefone($telefone){
        $sql = "DELETE FROM $this->table WHERE telefone = :telefone";
        $stmt = DB::prepare($sql);
        $stmt->bindParam(":telefone", $telefone);

        return($stmt->execute());
    }
}
?>