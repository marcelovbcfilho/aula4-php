<?php
    require_once "Crud.php";

    class TpcontatoDAO extends Crud{
        protected $table = "tpcontato";

        public function insert($tpcontato){
            $sql = "INSERT INTO $this->table (tipo) VALUES (:tipo)";
            $stmt = DB::prepare($sql);
            $stmt->bindValue(":tipo", $tpcontato->getTipo());

            return $stmt->execute();
        }

        public function update($id, $tpcontato){
            $sql = "UPDATE $this->table SET tipo = :tipo WHERE id = :id";
            $stmt = DB::prepare($sql);
            $stmt->bindValue(":tipo", $tpcontato->getTipo);
            $stmt->bindValue(":id", $id, PDO::PARAM_INT);

            return $stmt->execute();
        }

        public function findID($id){
            $sql = "SELECT * FROM $this->table WHERE id = :id";
            $stmt = DB::prepare($sql);
            $stmt->bindValue(":id", $id, PDO::PARAM_INT);
            $stmt->execute();

            return ($stmt->fetch());
        }
    }
?>
