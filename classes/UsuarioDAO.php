<?php
    require_once "Crud.php";

    class UsuarioDAO extends Crud{
        protected $table = "usuarios";

        public function insert($usuario){
        $sql = "INSERT INTO $this->table (nome, email, senha, perfil) VALUES (:nome, :email, :senha, :perfil)";
        $stmt = DB::prepare($sql);
        $stmt->bindValue(":nome", $usuario->getNome());
        $stmt->bindValue(":email", $usuario->getEmail());
        $stmt->bindValue(":senha", $usuario->getSenha());
        $stmt->bindValue(":perfil", $usuario->getPerfil());

        return $stmt->execute();
    }

    public function update($id, $usuario){
        $sql = "UPDATE $this->table SET nome = :nome, email = :email, perfil = :perfil WHERE id = :id";
        $stmt = DB::prepare($sql);
        $stmt->bindValue(":nome", $usuario->getNome());
        $stmt->bindValue(":email", $usuario->getEmail());
        $stmt->bindValue(":perfil", $usuario->getPerfil());
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);

        return $stmt->execute();
    }

    public function find_login($usuario){
        // :id = Significa que iremos utilizar a variável recebido como parâmetro.
        $sql = "SELECT * FROM $this->table WHERE email = :email  AND senha = :senha";
        $stmt = DB::prepare($sql);
        $stmt->bindValue(":email", $usuario->getEmail(), PDO::PARAM_STR);
        $stmt->bindValue(":senha", $usuario->getSenha(), PDO::PARAM_STR);
        $stmt->execute();

        return ($stmt->fetch());
    }
    }
?>