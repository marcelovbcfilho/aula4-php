<?php
require_once("tfpdf.php");
require_once("Usuarios.php");
require_once("Contatos.php");
require_once("ContatoDAO.php");
require_once("TelefonesDAO.php");

class PDF extends TFPDF
{
  private $contatos;

  function __construct($usuario)
  {
    parent::__construct();
    $contatoDAO = new ContatoDAO();
    $telefoneDAO = new TelefonesDAO();
    $contatosArray = array();

    // Salva todos os contatos do usuário em um array
    foreach ($contatoDAO->findAllCompleto() as $key => $value) {
      $contatoTemp = new Contatos();
      $idContato = $value->id;
      $contatoTemp->setNome($value->nome);
      $contatoTemp->setCelular($value->celular);
      $contatoTemp->setEmail($value->email);
      $contatoTemp->setApelido($value->apelido);
      $contatoTemp->setDataNascimento($value->data_nascimento);
      //$telefones = array();
      /*foreach ($telefoneDAO->find($idContato) as $telefone) {
        array_push($telefone->telefone);
      }*/
      //$contatoTemp->setTelefone(telefones);
      $contatoTemp->setTipo($value->tipo);
      array_push($contatosArray, $contatoTemp);
    }
    $this->contatos = $contatosArray;
  }

  // Page header
  function Header()
  {
    // Logo
    $this->Image('C:\wamp64\www\Aula04\classes\logo.png', 10, 6, 30);
    // Arial bold 15
    $this->SetFont('Times', 'B', 15);
    // Move to the right
    $this->Cell(80);
    // Title
    $this->Cell(30, 10, 'Contatos', 1, 0, 'C');
    // Line break
    $this->Ln(20);
  }

  // Page footer
  function Footer()
  {
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Times', 'I', 8);
    // Page number
    $this->Cell(0, 10, 'Página ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
  }

  // Colored table
  function FancyTable($header, $data)
  {
    // Colors, line width and bold font
    $this->SetFillColor(255, 0, 0);
    $this->SetTextColor(255);
    $this->SetDrawColor(128, 0, 0);
    $this->SetLineWidth(.3);
    $this->SetFont('', 'B');
    // Header
    $w = array(50, 45, 25, 25, 25, 20);
    for ($i = 0; $i < count($header); $i++)
      $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', true);
    $this->Ln();
    // Color and font restoration
    $this->SetFillColor(224, 235, 255);
    $this->SetTextColor(0);
    $this->SetFont('');
    // Data
    $fill = false;
    foreach ($data as $row) {
      $this->Cell($w[0], 6, $row->getNome(), 'LR', 0, 'L', $fill);
      $this->Cell($w[1], 6, $row->getEmail(), 'LR', 0, 'L', $fill);
      $this->Cell($w[2], 6, $row->getApelido(), 'LR', 0, 'R', $fill);
      $this->Cell($w[3], 6, $row->getCelular(), 'LR', 0, 'R', $fill);
      $origDate = $row->getDataNascimento();
      $newDate = date("d-m-Y", strtotime($origDate));
      $date = str_replace('-', '/', $newDate);
      $this->Cell($w[4], 6, $date, 'LR', 0, 'R', $fill);
      $this->Cell($w[5], 6, $row->getTipo(), 'LR', 0, 'R', $fill);
      $this->Ln();
      $fill = !$fill;
    }
    // Closing line
    $this->Cell(array_sum($w), 0, '', 'T');
  }

  function getContatos()
  {
    return $this->contatos;
  }
}
