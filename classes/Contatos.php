<?php

    class Contatos{
        private $nome;
        private $celular;
        private $email;
        private $apelido;
        private $data_nascimento;
        private $tipo ;
        private $usuario;
        private $telefone;

        public function setNome($nome){
            $this->nome = $nome;
        }

        public function setCelular($celular){
            $this->celular = $celular;
        }

        public function setEmail($email){
            $this->email = $email;
        }

        public function setApelido($apelido){
            $this->apelido = $apelido;
        }

        public function setDataNascimento($data_nascimento){
            $this->data_nascimento = $data_nascimento;
        }

        public function setTipo($tipo){
            $this->tipo = $tipo;
        }

        public function setUsuario($usuario){
            $this->usuario = $usuario;
        }

        public function setTelefone($telefone){
          $this->telefone = $telefone;
        }

        public function getNome(){
            return $this->nome;
        }

        public function getCelular(){
            return $this->celular;
        }

        public function getEmail(){
            return $this->email;
        }

        public function getApelido(){
            return $this->apelido;
        }

        public function getDataNascimento(){
            return $this->data_nascimento;
        }

        public function getTipo(){
            return $this->tipo;
        }

        public function getUsuario(){
            return $this->usuario;
        }

        public function getTelefone(){
          return $this->telefone;
        }
    }
?>
