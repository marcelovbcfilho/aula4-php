<?php
require_once "Crud.php";
require_once "TelefonesDAO.php";
require_once "Contatos.php";

class ContatoDAO extends Crud
{
    protected $table = "contatos";

    public function insert($contato)
    {
        $sql = "INSERT INTO $this->table (nome, celular, email, apelido, data_nascimento, tipo, usuario) VALUES (:nome, :celular, :email, :apelido, :data_nascimento, :tipo, :usuario)";
        $stmt = DB::prepare($sql);
        $stmt->bindValue(":nome", $contato->getNome());
        $stmt->bindValue(":celular", $contato->getCelular());
        $stmt->bindValue(":email", $contato->getEmail());
        $stmt->bindValue(":apelido", $contato->getApelido());
        $stmt->bindValue(":data_nascimento", $contato->getDataNascimento());
        $stmt->bindValue(":tipo", $contato->getTipo());
        $stmt->bindValue(":usuario", $_SESSION["usuario"]["idUsuario"], PDO::PARAM_INT);
        $stmt = $stmt->execute();
        $telefoneDAO = new TelefonesDAO();

        $lastId = DB::pegaId();

        foreach ($contato->getTelefone() as $telefone) {
            $telefoneDAO->insertTudo($lastId, $telefone);
        }

        return $stmt;
    }

    public function update($id, $contato)
    {
        $sql = "UPDATE $this->table SET nome = :nome, celular = :celular, email = :email, apelido = :apelido, data_nascimento = :data_nascimento, tipo = :tipo WHERE id = :id";
        $stmt = DB::prepare($sql);
        $stmt->bindValue(":nome", $contato->getNome());
        $stmt->bindValue(":celular", $contato->getCelular());
        $stmt->bindValue(":email", $contato->getEmail());
        $stmt->bindValue(":apelido", $contato->getApelido());
        $stmt->bindValue(":data_nascimento", $contato->getDataNascimento());
        $stmt->bindValue(":tipo", $contato->getTipo());
        $stmt->bindValue(":id", $id, PDO::PARAM_INT);
        $telefoneDAO = new TelefonesDAO();

        foreach ($contato->getTelefone() as $telefone) {
            $telefoneDAO->insertTudo($id, $telefone);
        }
        return $stmt->execute();
    }

    public function findAllCompleto()
    {
        $sql = "SELECT c.id, c.nome, c.apelido, c.celular, c.email, c.data_nascimento, t.tipo FROM $this->table c JOIN tpcontato t ON c.tipo = t.id WHERE c.usuario = :usuario";
        $stmt = DB::prepare($sql);
        $stmt->bindValue(":usuario", $_SESSION["usuario"]["idUsuario"], PDO::PARAM_INT);
        $stmt->execute();

        return ($stmt->fetchAll());
    }

    public function findGraficoData()
    {
        $sql = "SELECT EXTRACT(MONTH FROM data_nascimento) AS mes, COUNT(id) as quantidade FROM $this->table WHERE usuario = :usuario GROUP BY (EXTRACT(MONTH FROM data_nascimento));";
        $stmt = DB::prepare($sql);
        $stmt->bindValue(":usuario", $_SESSION["usuario"]["idUsuario"], PDO::PARAM_INT);
        $stmt->execute();

        $datas = array();
        $quantidade = array();
        foreach ($stmt->fetchAll() as $key => $value) {
            array_push($datas, $value->mes);
            array_push($quantidade, $value->quantidade);
        }
        $resultado = array();
        array_push($resultado, $datas, $quantidade);
        return $resultado;
    }
}
