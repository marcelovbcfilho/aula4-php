<?php
include_once "templates/header.php";
include_once "classes/Usuarios.php";
include_once "classes/UsuarioDAO.php";

// valida se o usuário está logado
$usuario = new Usuarios();
session_start();
if (isset($_SESSION["usuario"]["nomeUsuario"])) {
  $idCliente = $_SESSION["usuario"]["idUsuario"];
  $usuario->setNome($_SESSION["usuario"]["nomeUsuario"]);
  $usuario->setEmail($_SESSION["usuario"]["emailUsuario"]);
  $usuario->setPerfil($_SESSION["usuario"]["perfil"]);
} else {
  header("Location: index.php?login=SIM");
}

$alterou = 0;

// Variavel para atualizar os dados do usuario
$usuarioDAO = new UsuarioDAO();

if (isset($_POST['ATUALIZAR'])) {
  echo ("Entrei");
  if (isset($_POST['txtNome'])) {
    $usuario->setNome($_POST['txtNome']);
    if (isset($_POST['txtEmail'])) {
      $usuario->setEmail($_POST['txtEmail']);
      if (isset($_FILES['filePerfil']) and $_FILES['filePerfil'] != null) {
        echo ("Imagem achada");

        $image = $_FILES['filePerfil'];
        $nomeFinal = time() . '.jpg';
        echo ("Nome final imagem: " . $nomeFinal);

        if (move_uploaded_file($image['tmp_name'], $nomeFinal)) {
          $tamanhoImagem = filesize($nomeFinal);
          echo ("Tamanho da imagem: " . $tamanhoImagem);

          $mySqlImage = file_get_contents($nomeFinal);

          $usuario->setPerfil($mySqlImage);

          if ($usuarioDAO->update($idCliente, $usuario)) {
            $alterou = 1;
          }

          unlink($nomeFinal);

          $result = $usuarioDAO->find($idCliente);
          if (!empty($result)) {
            $_SESSION["usuario"]["idUsuario"] = $result->id;
            $_SESSION["usuario"]["nomeUsuario"] = $result->nome;
            $_SESSION["usuario"]["emailUsuario"] = $result->email;
            $image  = time() . ".jpg";
            file_put_contents($image, $result->perfil);
            $_SESSION["usuario"]["perfil"] = $image;
          }
        }
      }
    }
  }
}

?>
<!-- Side Navbar -->
<nav class="side-navbar">
  <div class="side-navbar-wrapper">
    <!-- Sidebar Header    -->
    <div class="sidenav-header d-flex align-items-center justify-content-center">
      <!-- User Info-->
      <div class="sidenav-header-inner text-center"><img src="<?php echo ($usuario->getPerfil()); ?>" alt="person" class="img-fluid rounded-circle">
        <h2 class="h5"><?php echo ($usuario->getNome()); ?></h2>
      </div>
      <!-- Small Brand information, appears on minimized sidebar-->
      <div class="sidenav-header-logo"><a href="inicio.php" class="brand-small text-center"> <strong><?php echo ($usuario->getNome()[0]); ?></strong><strong class="text-primary"><?php $segundoNome = explode(" ", $usuario->getNome());
                                                                                                                                                                                  echo ($segundoNome[1][0]); ?></strong></a></div>
    </div>
    <!-- Sidebar Navigation Menus-->
    <div class="main-menu">
      <h5 class="sidenav-heading">Menu</h5>
      <ul id="side-main-menu" class="side-menu list-unstyled">
        <li><a href="inicio.php"> <i class="icon-home"></i>Home </a></li>
        <li><a href="mostra-contato.php"> <i class="icon-grid"></i>Tables </a></li>
        <li class="active"><a href="alterar-dados.php"> <i class="icon-user"></i>Alterar Dados </a></li>
        <li><a href="grafico-contato.php"> <i class="fa fa-bar-chart"></i>Gráficos</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="page">
  <!-- navbar-->
  <header class="header">
    <nav class="navbar">
      <div class="container-fluid">
        <div class="navbar-holder d-flex align-items-center justify-content-between">
          <div class="navbar-header"><a id="toggle-btn" href="#" class="menu-btn"><i class="icon-bars"> </i></a><a href="index.html" class="navbar-brand">
              <div class="brand-text d-none d-md-inline-block"><span>Sistema | </span><strong class="text-primary"> PHP</strong></div>
            </a></div>
          <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
            <!-- Log out-->
            <li class="nav-item"><a href="logout.php" class="nav-link logout"> <span class="d-none d-sm-inline-block">Logout</span><i class="fa fa-sign-out"></i></a></li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
  <br />
  <section>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <h4>Alterar Dados</h4>
            </div>
            <div class="card-body">
              <div class="form-inner">
                <form class="text-left form-validate" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST" enctype="multipart/form-data">
                  <div class="form-group-material">
                    <div class="form-group-material">
                      <input type="text" id="txtNome" name="txtNome" required data-msg="Por favor informe o seu nome" class="input-material" />
                      <label for="txtNome" class="label-material">Nome</label>
                    </div>
                    <div class="form-group-material">
                      <input type="email" id="txtEmail" name="txtEmail" required data-msg="Por favor informe seu e-mail" class="input-material" />
                      <label for="txtEmail" class="label-material">E-mail</label>
                    </div>
                    <div class="form-group-material">
                      <label for="filePerfil" class="label-material">Foto de Perfil</label>
                      <input type="file" id="filePerfil" name="filePerfil" required data-msg="Por favor informe sua foto de perfil" class="input-material" />
                    </div>
                    <input type="hidden" id="ATUALIZAR" name="ATUALIZAR" value="SIM" />
                    <div class="form-group-material">
                      <button class="btn btn-default" type="reset" id="reset" name="reset">Limpar</button>
                      <button class="btn btn-primary" type="submit" id="salvar" name="salvar">Salvar</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="card-footer">

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>