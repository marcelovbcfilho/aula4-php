<?php include_once("templates/header.php"); ?>
<div class="page login-page">
  <div class="container">
    <div class="form-outer text-center d-flex align-items-center">
      <div class="form-inner">
        <div class="logo text-uppercase"><span>Cadastro no</span><strong class="text-primary"> Sistema</strong></div>
        <form class="text-left form-validate" method="POST" action="cadusuario.php"  enctype="multipart/form-data">
          <div class="form-group-material">
            <input id="txtNome" type="text" name="txtNome" required data-msg="Por favor informe o nome de usuário" class="input-material">
            <label for="txtNome" class="label-material">Nome de usuário</label>
          </div>
          <div class="form-group-material">
            <input id="txtEmail" type="email" name="txtEmail" required data-msg="Please enter a valid email address" class="input-material">
            <label for="txtEmail" class="label-material">E-mail </label>
          </div>
          <div class="form-group-material">
            <input id="txtSenha" type="password" name="txtSenha" required data-msg="Please enter your password" class="input-material">
            <label for="txtSenha" class="label-material">Senha </label>
          </div>
          <div class="form-group-material">
            <label for="filePerfil" class="label-material">Foto de perfil </label>
          <input id="filePerfil" type="file" name="filePerfil" required data-msg="Please enter your image perfil" class="input-material">
          </div>
          <div class="form-group text-center">
            <input id="register" type="submit" value="Registrar" class="btn btn-primary">
          </div>
        </form><small>Já tem uma conta ? </small><a href="index.php" class="signup">Login</a>
      </div>
      <div class="copyrights text-center">
        <p>Design by <a href="https://bootstrapious.com" class="external">Bootstrapious</a></p>
        <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
      </div>
    </div>
  </div>
</div>
<div class="d-inline-block">
  <h1>Cadastro de Cliente</h1>
  <form action="cadusuario.php" method="POST">
    <div class="form-group">
      <label for="txtNome">Nome</label>
      <input class="form-control" type="text" id="txtNome" name="txtNome" placeholder="LOGIN" />
    </div>
    <div class="form-group">
      <label for="txtEmail">E-Mail</label>
      <input class="form-control" type="text" id="txtEmail" name="txtEmail" placeholder="E-MAIL" />
    </div>
    <div class="form-group">
      <label for="txtSenha">Senha</label>
      <input class="form-control" type="password" id="txtSenha" name="txtSenha" placeholder="SENHA" />
    </div>

    <div class="btn-group-toggle">
      <input class="btn btn-success" type="submit" value="Cadastrar" />
  </form>
  <a class="btn btn-primary" onclick="
                document.getElementById('txtNome').value = '';
                document.getElementById('txtEmail').value = '';
                document.getElementById('txtSenha').value = '';">Limpar</a>
  <a class="btn btn-danger" href="index.php">Voltar</a>
</div>
</body>

</html>