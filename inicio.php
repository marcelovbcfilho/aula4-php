<?php include_once("templates/header.php"); ?>
<?php
require_once "classes/Usuarios.php";

$usuario = new Usuarios();
session_start();
if (isset($_SESSION["usuario"]["nomeUsuario"])) {
  $idCliente = $_SESSION["usuario"]["idUsuario"];
  $usuario->setNome($_SESSION["usuario"]["nomeUsuario"]);
  $usuario->setEmail($_SESSION["usuario"]["emailUsuario"]);
  $usuario->setPerfil($_SESSION["usuario"]["perfil"]);
} else {
  header("Location: login.php?login=SIM");
}
?>
<!-- Side Navbar -->
<nav class="side-navbar">
  <div class="side-navbar-wrapper">
    <!-- Sidebar Header    -->
    <div class="sidenav-header d-flex align-items-center justify-content-center">
      <!-- User Info-->
      <div class="sidenav-header-inner text-center"><img src="<?php echo($usuario->getPerfil()); ?>" alt="person" class="img-fluid rounded-circle">
        <h2 class="h5"><?php echo ($usuario->getNome()); ?></h2>
      </div>
      <!-- Small Brand information, appears on minimized sidebar-->
      <div class="sidenav-header-logo"><a href="inicio.php" class="brand-small text-center"> <strong><?php echo ($usuario->getNome()[0]); ?></strong><strong class="text-primary"><?php $segundoNome = explode(" ", $usuario->getNome());
                                                                                                                                                                                  echo ($segundoNome[1][0]); ?></strong></a></div>
    </div>
    <!-- Sidebar Navigation Menus-->
    <div class="main-menu">
      <h5 class="sidenav-heading">Menu</h5>
      <ul id="side-main-menu" class="side-menu list-unstyled">
        <li class="active"><a href="inicio.php"> <i class="icon-home"></i>Home </a></li>
        <li><a href="mostra-contato.php"> <i class="icon-grid"></i>Tables </a></li>
        <li><a href="alterar-dados.php"> <i class="icon-user"></i>Alterar Dados </a></li>
        <li><a href="grafico-contato.php"> <i class="fa fa-bar-chart"></i>Gráficos</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="page">
  <!-- navbar-->
  <header class="header">
    <nav class="navbar">
      <div class="container-fluid">
        <div class="navbar-holder d-flex align-items-center justify-content-between">
          <div class="navbar-header"><a id="toggle-btn" href="#" class="menu-btn"><i class="icon-bars"> </i></a><a href="index.html" class="navbar-brand">
              <div class="brand-text d-none d-md-inline-block"><span>Sistema | </span><strong class="text-primary"> PHP</strong></div>
            </a></div>
          <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
            <!-- Log out-->
            <li class="nav-item"><a href="logout.php" class="nav-link logout"> <span class="d-none d-sm-inline-block">Logout</span><i class="fa fa-sign-out"></i></a></li>
          </ul>
        </div>
      </div>
    </nav>
  </header>

  <br />
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <h4>Minha conta</h4>
          </div>
          <div class="card-body">
            <h2>Bem Vindo <?php echo ($usuario->getNome()); ?></h2>
            <h2>E-mail: <?php echo ($usuario->getEmail()); ?></h2>
            <img src="templates/wave.gif" />
          </div>
        </div>
      </div>
    </div>
    </body>

    </html>