<!-- Responsável por persistir os dados no banco -->
<?php
// Podemos utilizar o include, require, include_once, require_once = para utilizar outro arquivo php no atual
require_once "classes/Usuarios.php";
require_once "classes/UsuarioDAO.php";

$usuario = new Usuarios();
$usuarioDAO = new UsuarioDAO();

$usuario->setNome($_POST["txtNome"]);
$usuario->setEmail($_POST["txtEmail"]);
$usuario->setSenha($_POST["txtSenha"]);
if (isset($_FILES['filePerfil'])) {
    echo ("Imagem achada");

    $image = $_FILES['filePerfil'];
    $nomeFinal = time() . '.jpg';
    echo ("Nome final imagem: " . $nomeFinal);

    if (move_uploaded_file($image['tmp_name'], $nomeFinal)) {
        $tamanhoImagem = filesize($nomeFinal);
        echo ("Tamanho da imagem: " . $tamanhoImagem);

        $mySqlImage = file_get_contents($nomeFinal);;

        $usuario->setPerfil($mySqlImage);

        $result = $usuarioDAO->find_login($usuario);
        if (!empty($result)) {
            if (session_status() !== PHP_SESSION_ACTIVE) {
                session_cache_expire(60);
                session_start();
                $_SESSION["usuario"]["idUsuario"] = $result->id;
                $_SESSION["usuario"]["nomeUsuario"] = $result->nome;
                $_SESSION["usuario"]["emailUsuario"] = $result->email;
                $image  = time() . ".jpg";
                file_put_contents($image, $result->perfil);
                $_SESSION["usuario"]["perfil"] = $image;
                header("Location: inicio.php");
            }
        }
    }
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CADUSUARIO</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.css" />
</head>

<body>
    <?php
    if ($usuarioDAO->insert($usuario)) {
        header("Location: inicio.php");
    } else {
        header("Location: erro.php");
    }
    ?>
</body>

</html>