<!DOCTYPE html>
<div class="content">
    <html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Sistema | PHP</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <!-- Fontastic Custom icon font-->
        <link rel="stylesheet" href="css/fontastic.css">
        <!-- Google fonts - Roboto -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
        <!-- jQuery Circle-->
        <link rel="stylesheet" href="css/grasp_mobile_progress_circle-1.0.0.min.css">
        <!-- Custom Scrollbar-->
        <link rel="stylesheet" href="vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Toastr notification-->
        <link rel="stylesheet" href="css/toastr.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">
        <!-- Page icon -->
        <link rel="icon" type="image/ico" href="templates/icon.png" />
        <!-- Favicon-->
        <link rel="shortcut icon" href="icon.png">
        <!-- Gráficos google -->
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <!-- Scripts js -->
        <!-- JavaScript files-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/popper.js/umd/popper.min.js"> </script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="js/grasp_mobile_progress_circle-1.0.0.min.js"></script>
        <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
        <script src="vendor/chart.js/Chart.min.js"></script>
        <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
        <script src="vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="js/toastr.js"></script>
        <!-- Main File-->
        <script src="js/front.js"></script>
        <style>
            #loading {
                background: url('templates/loader.gif') no-repeat center center;
                position: absolute;
                top: 0;
                left: 0;
                height: 100%;
                width: 100%;
                z-index: 9999999;
            }

            #preenche {
                width: 10000px;
                height: 10000px;
                color white;
            }
        </style>

        <script>
            function hideLoader() {
                $('#loading').hide();
            }

            $(window).ready(hideLoader);

            // Strongly recommended: Hide loader after 20 seconds, even if the page hasn't finished loading
            setTimeout(hideLoader, 20 * 1000);
        </script>
    </head>

    <body>
        <div id="loading"></div>