<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Erro | Algo deu errado</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-grid.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-grid.css.map">
    <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-grid.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-grid.min.css.map">
    <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-reboot.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-reboot.css.map">
    <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-reboot.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-reboot.min.css.map">
    <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css.map">
    <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.min.css.map">
</head>

<body>
    <?php
    if (isset($_GET["login"])) {
        ?>
        <h1>Você precisa estar logado para acessar essa página!</h1>
        <a href="index.php">Voltar</a>
    <?php
} else {
    ?>
        <h1>Erro, desconhecido!</h1>
        <a href="index.php">Voltar</a>
    <?php
}
?>
    <script src="bootstrap.bundle.js"></script>
    <script src="bootstrap.bundle.js.map"></script>
    <script src="bootstrap.bundle.min.js"></script>
    <script src="bootstrap.bundle.min.js.map"></script>
    <script src="bootstrap.js"></script>
    <script src="bootstrap.bundle.js.map"></script>
    <script src="bootstrap.bundle.min.js"></script>
    <script src="bootstrap.bundle.min.js.map"></script>
</body>

</html>