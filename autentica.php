<?php
require_once "classes/Usuarios.php";
require_once "classes/UsuarioDAO.php";

$usuario = new Usuarios();
$usuarioDAO = new UsuarioDAO();

// Verifica se o login foi preenchido
if (isset($_POST["txtLogin"])) {
    $usuario->setEmail($_POST["txtLogin"]);
    // Se o login estiver preenchido verifica a senha
    if (isset($_POST["txtSenha"])) {
        $usuario->setSenha($_POST["txtSenha"]);
        $result = $usuarioDAO->find_login($usuario);
        if (!empty($result)) {
            if(session_status() !== PHP_SESSION_ACTIVE){
                session_cache_expire(60);
                session_start();
                $_SESSION["usuario"]["idUsuario"] = $result->id;
                $_SESSION["usuario"]["nomeUsuario"] = $result->nome;
                $_SESSION["usuario"]["emailUsuario"] = $result->email;
                $image  = time() . ".jpg";
                file_put_contents($image, $result->perfil);
                $_SESSION["usuario"]["perfil"] = $image;
                header("Location: inicio.php");
            }
        } else {
            header("Location: index.php?incorreto=SIM");
        }
    } else {
        header("Location: index.php?falta-senha=SIM");
    }
} else {
    header("Location: index.php?falta-login=SIM");
}
?>