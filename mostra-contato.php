<!-- Página inicial do sistema após o Login -->
<?php
require_once "classes/Contatos.php";
require_once "classes/Usuarios.php";
require_once "classes/Tpcontatos.php";
require_once "classes/ContatoDAO.php";
require_once "classes/TelefonesDAO.php";
require_once "classes/TpcontatoDAO.php";
require_once "classes/Usuarios.php";

// Import css e js
include_once("templates/header.php");

$salvou = 0;
$alterou = 0;
$excluiu = 0;

// valida se o usuário está logado
$usuario = new Usuarios();
session_start();
if (isset($_SESSION["usuario"]["nomeUsuario"])) {
  $idCliente = $_SESSION["usuario"]["idUsuario"];
  $usuario->setNome($_SESSION["usuario"]["nomeUsuario"]);
  $usuario->setEmail($_SESSION["usuario"]["emailUsuario"]);
  $usuario->setPerfil($_SESSION["usuario"]["perfil"]);
} else {
  header("Location: index.php?login=SIM");
}

$contatoDAO = new ContatoDAO();
$telefones = array();
$tpcontatoDAO = new TpcontatoDAO();
$telefoneDAO = new TelefonesDAO();

$contatoSalvar = new Contatos();
if (isset($_POST["idExcluir"])) {
  if ($contatoDAO->delete($_POST["idExcluir"])) {
    $excluiu = 1;
  }
}

if (isset($_POST["idAlterarContato"])) {
  if (isset($_POST["txtNome"])) {
    $contatoSalvar->setNome($_POST["txtNome"]);
  }

  if (isset($_POST["txtCelular"])) {
    $contatoSalvar->setCelular($_POST["txtCelular"]);
  }

  if (isset($_POST["txtEmail"])) {
    $contatoSalvar->setEmail($_POST["txtEmail"]);
  }

  if (isset($_POST["txtApelido"])) {
    $contatoSalvar->setApelido($_POST["txtApelido"]);
  }

  if (isset($_POST["dateDataNascimento"])) {
    // Convert HTML(type="date") into MySql Date type
    $input_date = $_POST["dateDataNascimento"];
    $date = date("Y-m-d", strtotime($input_date));
    $contatoSalvar->setDataNascimento($date);
  }

  // Se um telefone estiver setado entra
  if (isset($_POST["telefone-1"])) {
    // Passa pro todos os telefones que o contato criou
    for ($cont = 1, $acabou = true; $acabou; $cont++) {

      // Cria o id do telefone para recuperar
      $postTelefone = "telefone-" . $cont;

      // Se o telefone estiver setado ainda existem telefones para serem cadastrados
      if (isset($_POST[$postTelefone])) {

        // Se a varável ativo estiver setada entra
        if (isset($_POST["ativo-" . $postTelefone])) {

          // Se a variável ativo for == 0 ele precisa ser deletado
          if ($_POST["ativo-" . $postTelefone] == 0) {
            echo ("Deletar telefone: " + $_POST[$postTelefone]);
            $telefoneDAO->deleteTelefone($_POST[$postTelefone]);
          }

          // Se a variável ativo não estiver setada é um novo telefone e salva no array
        } else {
          array_push($telefones, $_POST[$postTelefone]);
        }

        // Se o telefone não estover setado acabaram os telefones
      } else {
        break;
      }
    }

    // Seta os telefones no contato
    $contatoSalvar->setTelefone($telefones);
  }

  if (isset($_POST["selectTipo"])) {
    $contatoSalvar->setTipo($_POST["selectTipo"]);
    if ($contatoDAO->update($_POST["idAlterarContato"], $contatoSalvar)) {
      $alterou = 1;
    }
  }
}

if (isset($_POST["salvar"])) {
  if (isset($_POST["txtNome"])) {
    $contatoSalvar->setNome($_POST["txtNome"]);
  }

  if (isset($_POST["txtCelular"])) {
    $contatoSalvar->setCelular($_POST["txtCelular"]);
  }

  if (isset($_POST["txtEmail"])) {
    $contatoSalvar->setEmail($_POST["txtEmail"]);
  }

  if (isset($_POST["txtApelido"])) {
    $contatoSalvar->setApelido($_POST["txtApelido"]);
  }

  if (isset($_POST["dateDataNascimento"])) {
    // Convert HTML(type="date") into MySql Date type
    $input_date = $_POST["dateDataNascimento"];
    $date = date("Y-m-d", strtotime($input_date));
    $contatoSalvar->setDataNascimento($date);
  }

  if (isset($_POST["telefone-1"])) {
    for ($cont = 1, $acabou = true; $acabou; $cont++) {
      $postTelefone = "telefone-" . $cont;
      if (isset($_POST[$postTelefone])) {
        array_push($telefones, $_POST[$postTelefone]);
      } else {
        $acabou = false;
      }
    }
    $contatoSalvar->setTelefone($telefones);
  }

  if (isset($_POST["selectTipo"])) {
    $contatoSalvar->setTipo($_POST["selectTipo"]);
    if ($contatoDAO->insert($contatoSalvar)) {
      $salvou = 1;
    }
  }
}

$contato = new Contatos();
$tipoContatos = new Tpcontatos();
?>
<!-- Side Navbar -->
<nav class="side-navbar">
  <div class="side-navbar-wrapper">
    <!-- Sidebar Header    -->
    <div class="sidenav-header d-flex align-items-center justify-content-center">
      <!-- User Info-->
      <div class="sidenav-header-inner text-center"><img src="<?php echo ($usuario->getPerfil()); ?>" alt="person" class="img-fluid rounded-circle">
        <h2 class="h5"><?php echo ($usuario->getNome()); ?></h2>
      </div>
      <!-- Small Brand information, appears on minimized sidebar-->
      <div class="sidenav-header-logo"><a href="inicio.php" class="brand-small text-center"> <strong><?php echo ($usuario->getNome()[0]); ?></strong><strong class="text-primary"><?php $segundoNome = explode(" ", $usuario->getNome());
                                                                                                                                                                                  echo ($segundoNome[1][0]); ?></strong></a></div>
    </div>
    <!-- Sidebar Navigation Menus-->
    <div class="main-menu">
      <h5 class="sidenav-heading">Menu</h5>
      <ul id="side-main-menu" class="side-menu list-unstyled">
        <li><a href="inicio.php"> <i class="icon-home"></i>Home </a></li>
        <li class="active"><a href="mostra-contato.php"> <i class="icon-grid"></i>Tables </a></li>
        <li><a href="alterar-dados.php"> <i class="icon-user"></i>Alterar Dados </a></li>
        <li><a href="grafico-contato.php"> <i class="fa fa-bar-chart"></i>Gráficos</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="page">
  <!-- navbar-->
  <header class="header">
    <nav class="navbar">
      <div class="container-fluid">
        <div class="navbar-holder d-flex align-items-center justify-content-between">
          <div class="navbar-header"><a id="toggle-btn" href="#" class="menu-btn"><i class="icon-bars"> </i></a><a href="index.html" class="navbar-brand">
              <div class="brand-text d-none d-md-inline-block"><span>Sistema | </span><strong class="text-primary"> PHP</strong></div>
            </a></div>
          <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
            <!-- Log out-->
            <li class="nav-item"><a href="logout.php" class="nav-link logout"> <span class="d-none d-sm-inline-block">Logout</span><i class="fa fa-sign-out"></i></a></li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
  <br />
  <section>
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <h4>Contatos</h4>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Nome</th>
                      <th scope="col">Exibir Detalhes</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($contatoDAO->findAllCompleto() as $key => $value) : $idContato = $value->id; ?>
                      <tr>
                        <th scope="row"><?php echo ("$idContato"); ?></th>
                        <td><?php echo ("$value->nome"); ?></td>
                        <td><button class="btn" type="button" onclick="mostraDetalhes('table-<?php echo ($idContato); ?>');"><i class="icon-bars"></i></button></td>
                      </tr>
                    <tbody style="display: none;" id="table-<?php echo ($idContato); ?>" name="table-<?php echo ($idContato); ?>">
                      <tr>
                        <td scope="col">Celular: <?php echo ("$value->celular"); ?></td>
                        <td scope="col">Ações: <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                            <input type="hidden" id="idAlterar" name="idAlterar" value="<?php echo ("$idContato"); ?>" />
                            <button type="submit" class="btn btn-primary">Alterar</button>
                          </form>
                          <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                            <input type="hidden" id="idExcluir" name="idExcluir" value="<?php echo ("$idContato"); ?>" />
                            <button type="submit" class="btn btn-primary">Excluir</button>
                          </form>
                        </td>
                      </tr>
                      <tr>
                        <td scope="col">E-Mail: <?php echo ("$value->email"); ?></td>
                      </tr>
                      <tr>
                        <td scope="col">Apelido: <?php echo ("$value->apelido"); ?></td>
                      </tr>
                      <tr>
                        <td scope="col">Data de Nascimento: <?php echo ("$value->data_nascimento"); ?></td>
                      </tr>
                      <tr>
                        <td scope="col">Telefones: <br /><?php foreach ($telefoneDAO->find($idContato) as $telefone) :
                                                            echo ("$telefone->telefone" . "<br/>");
                                                          endforeach ?></td>
                      </tr>
                      <tr>
                        <td scope="col">Tipo: <?php echo ("$value->tipo"); ?></td>
                      </tr>
                    </tbody>
                  <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="card-footer">
              <!-- Button trigger modal -->
              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#cadastrar-modal">
                Cadastrar Usuário
              </button>
              <a href="gera-pdf.php" class="btn btn-secondary">
                Gerar PDF
              </a>
            </div>
          </div>
        </div>
      </div>
  </section>
</div>
<div class="modal fade" id="cadastrar-modal" tabindex="-1" role="dialog" aria-labelledby="cadastrar-modal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Contato</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php if (isset($_POST["idAlterar"])) {
          $contatoAlterar = $contatoDAO->find($_POST["idAlterar"]); ?>
          <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
            <div class="form-group">
              <label for="txtNome">Nome</label>
              <input type="text" class="form-control" id="txtNome" name="txtNome" placeholder="Nome" <?php echo ("value='$contatoAlterar->nome'"); ?> />
            </div>
            <div class="form-group">
              <label for="txtCelular">Celular</label>
              <input type="text" class="form-control" id="txtCelular" name="txtCelular" placeholder="Celular" <?php echo ("value='$contatoAlterar->celular'"); ?> />
            </div>
            <div class="form-group">
              <label for="txtEmail">E-Mail</label>
              <input type="email" class="form-control" id="txtEmail" name="txtEmail" placeholder="E-Mail" <?php echo ("value='$contatoAlterar->email'"); ?> />
            </div>
            <div class="form-group">
              <label for="txtApelido">Apelido</label>
              <input type="text" class="form-control" id="txtApelido" name="txtApelido" placeholder="Apelido" <?php echo ("value='$contatoAlterar->apelido'"); ?> />
            </div>
            <div class="form-group">
              <label for="dateDataNascimento">Data Nascimento</label>
              <input type="date" class="form-control" id="dateDataNascimento" name="dateDataNascimento" placeholder="data" <?php echo ("value='$contatoAlterar->data_nascimento'"); ?> />
            </div>
            <div class="form-group">
              <button type="button" class="btn btn-default" name="button" onclick="adicionaTelefoneAlterar();">Adicionar Telefone</button>
            </div>
            <div class="form-froup" id="div-telefones">
              <?php
              $cont = 0;
              foreach ($telefoneDAO->find($_POST["idAlterar"]) as $telefone) :
                $cont += 1;
                ?>
                <label for="telefone-<?php echo ($cont); ?>">Telefone <?php echo ($cont); ?></label>
                <input type="text" id="telefone-<?php echo ($cont); ?>" name="telefone-<?php echo ($cont); ?>" value="<?php echo ($telefone->telefone); ?>" />
                <input type="hidden" id="ativo-telefone-<?php echo ($cont); ?>" name="ativo-telefone-<?php echo ($cont); ?>" value="1" />
                <button class="btn btn-default" type="button" onclick="desativarTelefone('telefone-<?php echo ($cont); ?>');">-</button>
                <br />
              <?php
            endforeach
            ?>

            </div>
            <input type="hidden" id="qtdeTelefones" name="qtdeTelefones" value="<?php echo ($cont); ?>" />
            <div class="form-group">
              <select class="custom-select" id="selectTipo" name="selectTipo">
                <?php
                $tpcontato = new Tpcontatos();
                foreach ($tpcontatoDAO->findAll() as $key => $value) {
                  if ($contatoAlterar->tipo == $value->id) {
                    echo ("<option value='" . $value->id . "' selected >" . $value->tipo . "</option>");
                  } else {
                    echo ("<option value='" . $value->id . "'>" . $value->tipo . "</option>");
                  }
                }
                ?>
              </select>
            </div>
            <input type="hidden" id="idAlterarContato" name="idAlterarContato" <?php echo ("value='$contatoAlterar->id'"); ?>>
            <button type="reset" class="btn btn-secondary">Limpar</button>
            <button type="submit" class="btn btn-primary">Salvar</button>
          </form>
        <?php
      } else { ?>
          <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
            <div class="form-group">
              <label for="txtNome">Nome</label>
              <input type="text" class="form-control" id="txtNome" name="txtNome" placeholder="Nome" />
            </div>
            <div class="form-group">
              <label for="txtCelular">Celular</label>
              <input type="text" class="form-control" id="txtCelular" name="txtCelular" placeholder="Celular" />
            </div>
            <div class="form-group">
              <label for="txtEmail">E-Mail</label>
              <input type="email" class="form-control" id="txtEmail" name="txtEmail" placeholder="E-Mail" />
            </div>
            <div class="form-group">
              <label for="txtApelido">Apelido</label>
              <input type="text" class="form-control" id="txtApelido" name="txtApelido" placeholder="Apelido" />
            </div>
            <div class="form-group">
              <label for="dateDataNascimento">Data Nascimento</label>
              <input type="date" class="form-control" id="dateDataNascimento" name="dateDataNascimento" placeholder="data" />
            </div>
            <div class="form-group">
              <button type="button" class="btn btn-default" name="button" onclick="adicionaTelefone();">Adicionar Telefone</button>
            </div>
            <div class="form-froup" id="div-telefones">
              <label for="telefone-1">Telefone 1</label>
              <input type="text" id="telefone-1" name="telefone-1" />
              <br />
            </div>
            <div class="form-group">
              <select class="custom-select" id="selectTipo" name="selectTipo">
                <?php $tpcontato = new Tpcontatos();
                foreach ($tpcontatoDAO->findAll() as $key => $value) {
                  echo ("<option value='" . $value->id . "'>" . $value->tipo . "</option>");
                }
                ?>
              </select>
            </div>
            <input type="hidden" id="salvar" name="salvar" value="salvar" />
            <button type="reset" class="btn btn-secondary">Limpar</button>
            <button type="submit" class="btn btn-primary">Salvar</button>
          </form>
        <?php
      } ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  <?php
  if ($excluiu == 1) {
    echo ("toastr.info('Contato excluido com sucesso!');");
  }

  if ($salvou == 1) {
    echo ("toastr.success('Contato salvo com sucesso!');");
  }

  if ($alterou == 1) {
    echo ("toastr.success('Contato alterado com sucesso!');");
  }
  ?>
  var telefone = 1;
  var divTelefone = document.getElementById("div-telefones");
  let input;
  let label;
  var telefonesAlterar = Number(document.getElementById("qtdeTelefones").value);

  function adicionaTelefone() {
    telefone += 1;
    label = document.createElement("label");
    label.setAttribute("for", "telefone-" + telefone);
    label.innerHTML = "Telefone " + telefone + " ";
    input = document.createElement("input");
    input.setAttribute("type", "texp");
    input.setAttribute("id", "telefone-" + telefone);
    input.setAttribute("name", "telefone-" + telefone);
    divTelefone.appendChild(label);
    divTelefone.appendChild(input);
    divTelefone.appendChild(document.createElement("br"));
  }

  function adicionaTelefoneAlterar() {
    telefonesAlterar += 1;
    label = document.createElement("label");
    label.setAttribute("for", "telefone-" + telefonesAlterar);
    label.innerHTML = "Telefone " + telefonesAlterar + " ";
    input = document.createElement("input");
    input.setAttribute("type", "texp");
    input.setAttribute("id", "telefone-" + telefonesAlterar);
    input.setAttribute("name", "telefone-" + telefonesAlterar);
    divTelefone.appendChild(label);
    divTelefone.appendChild(input);
    divTelefone.appendChild(document.createElement("br"));
  }

  function mostraDetalhes(tableDetalhes) {
    let tableContato = document.getElementById(tableDetalhes);
    if (tableContato.style.display == "none") {
      $('#' + tableDetalhes).fadeIn();
    } else {
      $('#' + tableDetalhes).fadeOut();
    }
  }

  function desativarTelefone(idTelefone) {
    let inputTelefone = document.getElementById(idTelefone);
    if (inputTelefone.style.display == "none") {
      inputTelefone.style.display = "block";
      document.getElementById("ativo-" + idTelefone).value = 1;
    } else {
      inputTelefone.style.display = "none";
      document.getElementById("ativo-" + idTelefone).value = 0;
    }
  }
  <?php
  if (isset($_POST["idAlterar"])) {
    echo ("$(window).on('load',function(){
         $('#cadastrar-modal  ').modal('show');
    });");
  }
  ?>
</script>
</body>

</html>