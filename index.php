    <?php include_once("templates/header.php"); ?>
    <div style="padding-left: 15%" class="page login-page">
      <div class="container">
        <div class="form-outer text-center d-flex align-items-center">
          <div class="form-inner">
            <div class="logo text-uppercase"><span>Sistema |</span><strong class="text-primary"> PHP</strong></div>
            <form method="post" action="autentica.php" class="text-left form-validate">
              <div class="form-group-material">
                <input id="txtLogin" type="email" name="txtLogin" required data-msg="Por favor informe seu e-mail" class="input-material">
                <label for="txtLogin" class="label-material">E-Mail</label>
                <?php
                if (isset($_GET["falta-login"])) {
                  echo ("<p style='color: red;text-align: center;'>Por favor informe o e-mail</p>");
                }
                ?>
              </div>
              <div class="form-group-material">
                <input id="txtSenha" type="password" name="txtSenha" required data-msg="Por favor informe sua senha" class="input-material">
                <label for="txtSenha" class="label-material">Senha</label>
              </div>
              <?php
              if (isset($_GET["falta-senha"])) {
                echo ("<p style='color: red;text-align: center;'>Por favor informe a senha</p>");
              }
              ?>
              <div class="form-group text-center">
                <button type="submit" id="login" class="btn btn-primary">Login</a>
                  <!-- This should be submit button but I replaced it with <a> for demo purposes-->
              </div>
            </form>
            <a href="#" class="forgot-pass">Forgot Password?</a>
            <small>Não tem uma conta? </small><a href="cadastro.php" class="signup">Cadastre-se</a>
          </div>
        </div>
      </div>
    </div>
    <script>
      <?php
      if (isset($_GET['incorreto'])) {
        echo ("toastr.error('Login ou senha informados incorretos');");
      }

      if (isset($_GET['logout'])) {
        echo ("toastr.success('Você saiu com sucesso');");
      }

      if (isset($_GET['nao-conectado'])) {
        echo ("toastr.error('Usuário não conectado');");
      }

      if (isset($_GET['login'])) {
        echo ("toastr.warning('É preciso estar conectado para acessar aquela página!');");
      }
      ?>
    </script>
  </body>
</html>