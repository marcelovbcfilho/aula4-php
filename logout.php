<?php
session_start();
unlink($_SESSION["usuario"]["perfil"]);
$_SESSION = array();
if (session_destroy()) {
  header("Location: index.php?logout=SIM");
} else {
  header("Location: index.php?nao-conectado=SIM");
}
